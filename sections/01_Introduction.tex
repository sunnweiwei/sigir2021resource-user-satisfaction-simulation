% !TEX root = ../main.tex

\section{Introduction}

\begin{figure}[t]
 \centering
 \includegraphics[width=0.93\columnwidth]{figures/figure_task.pdf}
 \vspace*{0.5\baselineskip}
 \caption{(a) Previous work on user simulation; (b) previous work on user satisfaction prediction; (c) our proposed task: simulating user satisfaction for evaluating task-oriented dialogues systems. We leave utterance generation (dotted line) as future work.}
 \label{figure:task}
\end{figure}

Task-oriented systems are developed to help users solve a specific task as efficiently as possible~\cite{Young2013POMDP}.
Evaluation is a crucial part of the development process of task-oriented dialogue systems.
For evaluating the performance of each module of a dialogue system, human evaluation, user satisfaction modeling, corpus-based approaches, and user simulation have all been leveraged~\citep{Deriu2020SurveyOE}.
Human evaluation through in-field experiments \citep{Lamel2000TheLA,Black2011SpokenDC} or crowd-sourcing \citep{Jurccek2011RealUE} is considered to reflect the overall performance of the system in a real-world scenario, but it is intrusive, time-intensive, and does not scale \citep{Deriu2020SurveyOE}.
User satisfaction modeling can be an alternative; it aims to automatically estimate user satisfaction based on human-machine interaction log data, but still requires human involvement.
To evaluate a dialogue system fully automatically, offline evaluation based on test sets is commonly used. However, this method is limited to a single turn and does not inform us about the overall usefulness of the system or about users' satisfaction with the flow of the dialogue~\citep{Zhang2020EvaluatingCR}. 
Therefore, evaluation results of offline methods have limited consistency with the results of human evaluation.
Simulation-based evaluation methods address the issues listed above; they are a viable choice for large-scale automatic evaluation~\citep{Deriu2020SurveyOE}.
User simulations can be used to evaluate functionalities of dialogue systems and they can serve as an environment to train reinforcement learning-based systems~\citep{Deriu2020SurveyOE}, leveraging agenda-based~\citep{Schatzmann2007AgendaBasedUS} or model-based simulation~\citep{Asri2016ASM}.
%
Building human-like user simulation is still an open challenge~\citep{Jannach2020ASO}.

To bridge the gap between human evaluation and user simulation, we attempt to combine user simulation with user satisfaction (cf.~Figure~\ref{figure:task}).
To this end, we first look into existing task-oriented dialogues and carry out a user study to investigate the characteristics of user satisfaction.
We arrive at two main observations:
\begin{enumerate*}[label=(\arabic*)]
\item \emph{User dissatisfaction is mainly caused by the system's failure in meeting the user's needs}.
Specifically, 36\% of the conversations are labeled as \emph{very dissatisfied}  because the system does not understand the user's needs, and 43\% are because the system understands the user's problems but cannot provide proper solutions. 
Figure~\ref{figure:example} illustrates the scenario.
\item \emph{Different degrees of satisfaction result in different sequences of user actions}.
For example, the right-side user in Figure~\ref{figure:example} may switch to customer service or explain further when encountering the same failed system reply in the context of different emotions.
We convert this intuition to a hypothesis that we verify by checking the records in the corpus.
When faced with a dialogue system's failure in understanding user needs, about 17.1\% of all users will switch to manual customer service, and about 64.3\% and 9.7\% will continue by providing additional information, or quit the conversation, respectively. This observation suggests that user simulation should work differently in different user satisfaction scenarios.
\end{enumerate*}

Informed by the observations just listed, we propose a novel task: \emph{to simulate user satisfaction for the evaluation of task-oriented dialogue systems}.
Figure~\ref{figure:task} illustrates the main difference between our task and previous work.
We extend the evaluation capability of user simulations and make the simulation more human-like by incorporating user satisfaction prediction and user action prediction. 

To facilitate research on user satisfaction simulation,
% As a novel task, 
% there is no existing dataset. 
% Work on interaction assessment~\citep{Schmitt2015InteractionQA} cannot be directly used in this task \mdr{because \ldots}.
% Therefore, 
we develop a user satisfaction annotation dataset, \acfi{USS}. We invite 40 annotators to label both the dialogue level and exchange level user satisfaction of 5 commonly used task-oriented dialogue datasets in different domains. 
This results in a dataset of 6,800 dialogues, where each individual user utterance, as well as each complete dialogue, is labeled on a 5-point satisfaction scale. 
Each dialogue is labeled by 3 annotators; the expert ratings are highly correlated, with a Fleiss Kappa score of 0.574.
The \ac{USS} dataset shares some characteristics with existing datasets for user satisfaction, but also differs in important ways (see Table~\ref{table:dataset-comparision}):
\begin{enumerate*}
\item Our user satisfaction labeling occurs before the user utterance, and is based on the dialogue context between user and system instead of the satisfaction expressed in the user's utterance.
\item The \ac{USS} dataset includes multiple domains, such as e-commerce, reservations, recommendations, etc.
\item The \ac{USS} dataset exceeds existing user satisfaction data in scale.
\end{enumerate*}

We share three baseline approaches to perform satisfaction prediction and user action prediction based on the newly collected data in \ac{USS}: a feature-based method, a hierarchical GRU-based method, and a BERT-based method. 
Experimental results suggest that distributed representations outperform feature-based methods. 
The hierarchical GRU-based method achieves the best performance in in-domain user satisfaction prediction, while the BERT-based method has a better cross-domain generalization ability thanks to the pre-training. 
We also show that the BERT-based method achieves state-of-the-art performance on the action prediction task.

In summary, this paper makes the following contributions:
\begin{enumerate*}[label=(\arabic*)]
\item We propose the novel task of simulating user satisfaction for the evaluation of task-oriented dialogue systems.
\item We collect and share a dataset, \ac{USS}, that includes 6,800 annotated dialogues in multiple domains.
\item We introduce three baseline methods for the tasks of satisfaction prediction and action prediction using the \ac{USS} dataset.
\end{enumerate*}

\begin{figure}[t]
 \centering
 \includegraphics[width=0.95\columnwidth]{figures/figure_1_4.pdf}
 \vspace*{0.75\baselineskip}
 \caption{Two examples of dialogues in the JDDC dataset~\citep{Chen2020TheJC},
 with different degrees of user satisfaction. 
 %
 The right-side system fails to understand the user's needs, and causes the user to be dissatisfied emotions and have a poor user experience. The left-side dialogue demonstrates an opposite case.}
 \label{figure:example}
 \vspace*{-0.25\baselineskip}
\end{figure}