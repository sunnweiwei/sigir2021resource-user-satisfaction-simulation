% !TEX root = ../main.tex

\section{Task formulation}
\label{sec:tr}
% To formulate our proposed task, we start to investigate our data collection to answer the following questions. 
To formulate the task of simulating user satisfaction, we first carry out a user study to explore the characteristics of user satisfaction in task-oriented dialogues.
% We conduct a questionnaire based on our data collection. 
Specifically, we invite 12 experts and let each expert annotate 20 dialogues sampled from the JDDC dataset; we used the JDDC dataset since it is more realistic than data constructed by the Wizard-of-Oz approach.
We ask each expert to score the user satisfaction for each dialogue turn and the entire conversation. In addition, a rational explanation is requested. 
We ask the experts to judge the user action changes after a change in satisfaction.
Based on this study, we answer the following questions:

\begin{enumerate*}[label=(\arabic*)]
\item \emph{What causes the user's dissatisfaction?}
%
% The user’s dissatisfaction mainly results from $4$ aspects: (a) \emph{the system does not meet the user’s needs}, (b) \emph{the user does not approve the system’s response}, (c) \emph{the system does not respond in time}, and, (d) \emph{the user is unhappy}.
%
We collect the results and find that, although annotators are satisfied with the system overall, about 12\% of the dialogue turns are labeled as unsatisfying. 
This indicates that there are fluctuations in user satisfaction when interacting with the system.
We analyze the annotators' explanations and find that the main reason for dissatisfaction relates to the system's failure to understand the user's needs or handling the user's requests.
Specifically, 36\% of all conversations labeled as \emph{very dissatisfied} are because \emph{the system does not understand the user's needs}, whereas 43\% are because \emph{the user does not approve the system's response}. In 64\% of the data, users had a bad user experience because \emph{the system was not professional enough or did not respond in time}.
Figure~\ref{figure:example} illustrates the scenario where the system does not understand the user's needs and causes low user satisfaction.

\item \emph{How does user satisfaction influence the user's behavior?}
Different degrees of satisfaction result in different sequences of user actions. 
Specifically, when encountering a failure in the a dialogue system's understanding of user needs, about 17.1\% of all users  \emph{switch to manual customer service}, and about 64.3\% and 9.7\% continue by \emph{providing additional information}, or \emph{quit the conversation}, respectively. 
Figure~\ref{figure:example} shows an example, where the right-side user switches to customer service or explains further when encountering the same failed system reply in light of different degrees of satisfaction.
Apart from user actions, we also observe changes such as attitude and information-seeking goal.
\end{enumerate*}

%\noindent%
The above observations indicate that predicting the fluctuations of user satisfaction during interaction is non-trivial.
% for user simulation, and it is difficult for previous rule-based methods to capture such changes.
%
Thus, we formulate our research task, i.e., to \emph{simulate user satisfaction for the evaluation of task-oriented dialogue systems}. 

This simulation task focuses on the prediction of the next user action as well as user satisfaction. 
Suppose that we have a dataset $\mathcal{D} = \{(U_{i}, a_{i}, s_{i})\}_{i=1}^{N}$,  where for all $i \in [1,N]$, $U_{i}$ is the dialogue context, $a_{i}$ is the next-turn user action, and $s_{i}$ denotes user satisfaction.
The task objective is to learn a classification model $P(a, s \mid U)$ from $\mathcal{D}$, and thus given a dialogue context $U$, it predicts the next-turn user action $a$ and user satisfaction $s$ based on $P(a, s \mid U)$.
The purpose of the task to increase the evaluation power of user simulations and to make the simulation more human-like by incorporating the user's potential changes in satisfaction in a simulator. 

% \noindent
% \textbf{RQ1:} \emph{What causes the user's dissatisfaction?}

% \noindent To make the conversational agent generate more human-like responses, emotion signals have been successfully applied to open-domain dialogue system~\cite{Ma2020ASO}. 
% However, only a few studies exist in the task of task-oriented dialogue generation~\cite{Devillers2003EmotionDI,Huang2020DevelopingEH,Zhang2020TowardsEU}.
% Inspired by previous work on emotion-aware dialogue generation~\cite{Rashkin2019TowardsEO}, we consider incorporating emotions into the user simulation paradigm to enhance the effectiveness of the evaluation, i.e., we focus on perceiving the user's emotional changes during the user simulation in evaluating task-oriented dialogues.
% To this end, we use an emotion classifier to investigate the distribution of emotion in two types of dialogues, i.e., chitchat dialogues and task-oriented dialogues. The emotion classifier is pre-trained on an annotated dataset collected from Reddit~\cite{Demszky2020GoEmotionsAD}. Table~\ref{table:emotion} illustrates the proportion of positive emotions against the negative ones in $2$ chitchat datasets (i.e., EDial~\cite{Rashkin2019TowardsEO} and CChat~\cite{Huang2017OverviewOT}) and $5$ task-oriented dialogues dataset (i.e., DSTC6 \cite{Bordes2017LearningEG}, JDDC \cite{Chen2020TheJC}, WOZ \cite{Budzianowski2018MultiWOZA}, SGD \cite{Rastogi2020TowardsSM}, MWOZ \cite{Eric2020MultiWOZ2A}). 
% In general, we find users in task-oriented dialogues show fewer emotions than in chit-chat scenarios. Specifically, the proportion of positive emotions is below 5\% in chit-chat dialogues, while such a number is above 15\% in task-oriented dialogues.
% Besides, different task-oriented dialogue corpora have different emotion proportions. 
% % \todo{In terms of data construction methods}
% In terms of data construction methods, we find that the real-world conversation data (e.g., JDDC) has more emotions than the data collected by crowdsourcing (e.g., WOZ, SGD, and MWOZ).
% According to the task of the dialogues, we find that users show more negative emotions in restaurant reservation dialogues (e.g., DSTC6 and WOZ).
% Although there are fewer negative emotions in task-oriented dialogue, we found that about 10\% of the data is still considered to be negative emotions, and these bad emotions can lead to bad user satisfaction.

% \begin{table}[]
% \centering
% \setlength\tabcolsep{2pt}
% \caption{Ratio of positive vs.\ against negative emotions in chit-chat and task-oriented dialogues.}
% \label{table:emotion}
% \begin{tabular}{l  rr  rrrrr}
% \toprule
%         & EDial    & CChat   & DSTC6  & JDDC & WOZ & SGD & MWOZ\\ 
% \midrule
% Ratio       & 3.2     &  4.1        &  15.1     & 18.0  & 21.3 &  38.6 & 39.8 \\
% \bottomrule
% \end{tabular}
% \end{table}

% \noindent
% \textbf{RQ2:} \emph{What are the characteristics of user satisfaction in task-oriented dialogue?}

% \noindent To explore the characteristics of user satisfaction in task-oriented dialogues, we conduct a questionnaire based on our data collection. 
% Specifically, we invite 10 experts and let each expert annotate 20 dialogues sampled from the JDDC corpus (we conduct user study on JDDC since it is more real than data constructed by the Wizard-of-Oz approach).
% We ask each expert to score the user satisfaction of each sentence in each dialogue with a rational explanation. 
% Besides, we ask the experts to judge the user action changes after a change in satisfaction.
% % Table~\ref{table:emotion-action} shows the results of our questionnaire. 
% Two main observations can be found according to the results:
% (1) The user’s dissatisfaction mainly results from $4$ aspects: (a) \emph{the system does not meet the user’s needs}, (b) \emph{the user does not approve the system’s response}, (c) \emph{the system does not respond in time}, and, (d) \emph{the user is unhappy}.
% (2) Different degrees of satisfaction result in a sequence of user actions. Given the same condition (e.g., misunderstanding user needs), about 39\% will switch to manual customer service, and about 41\% and 11\% will continue by providing additional information or quit the conversation. 
% Figure.\ref{figure:example} shows an example, where the right-side user switches to customer service or explains further when encountering the same failed system reply in the light of different degrees of satisfaction.
